<!--
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3
-->

# Authorization

**Authorization** is a blockchain module built using Cosmos SDK and Tendermint and created
with [ignite](https://github.com/ignite/cli). It can be used to manage application roles. It can create new roles and
access or update these roles. Typically another custom module will check the authorization module for the validity of an
application role and use the result in it’s own module.

The **Authorization** module can be used with
the [TokenManger](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/tokenmanager)
or with another custom module.

## Cloning and updating of the repository

The **Authorization** repository can be cloned and included in a custom project. To see how to clone and update the
Authorization module with the Token Manager and its other modules all at once see the readme of
the [TokenManger](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/tokenmanager)
.

## Documentation

The [documentation](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/documentation/-/blob/main/index.adoc)
provides extensive information about the Token Manager and its components, including the module **Authorization**.

## Installation and Configuration

To use the module **Authorization** refer to the documentation of the tendermint setup the module is being used with. In
case of the Token Manager, the installation and setup is described
in [chapter 12](git@git.openlogisticsfoundation.org:silicon-economy/base/blockchainbroker/digital-folder/documentation/-/blob/main/index.adoc)
of its documentation.

## Launch

To launch your blockchain live on multiple nodes use `ignite network` commands. Learn more
about [Starport Network](https://github.com/tendermint/spn).

## CLI commands

The following commands are supported by the Authorization module.

### Transactions

The following transaction commands are supported. If the module is used as part of the Token Manager, all transaction
commands need a preceding `TokenManagerd tx Authorization `

* `create-blockchain-account [id]` to create a blockchain account
* `deactivate-blockchain-account [id]` to deactivate a blockchain account
* `grant-application-role [user] [role-id]` to grant an application role to a blockchain account
* `revoke-application-role [account] [role-id]` to revoke an application role from a blockchain account
* `create-application-role [name] [description]` to create an application role
* `update-application-role [id] [description]` to update an application role
* `delete-application-role [id]` to delete an application role
* `deactivate-application-role [id]` to deactivate an application role
* `update-configuration [id] [permission-check]` to update a configuration

## Learn more

- [ignite](https://github.com/ignite/cli)
- [Cosmos SDK documentation](https://docs.cosmos.network)
- [Cosmos SDK Tutorials](https://tutorials.cosmos.network)
- [Discord](https://discord.gg/W8trcGV)
