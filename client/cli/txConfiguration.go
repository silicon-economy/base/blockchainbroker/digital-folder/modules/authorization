// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package cli

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cobra"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func CmdUpdateConfiguration() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "update-configuration [id] [permission-check]",
		Short: "Updates a configuration",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {

			id, _ := strconv.ParseUint(args[0], 10, 64)
			permissionCheck, _ := strconv.ParseBool(args[1])

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgUpdateConfiguration(
				clientCtx.GetFromAddress().String(),
				id,
				permissionCheck,
			)
			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
