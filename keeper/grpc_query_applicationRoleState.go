// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func (k Keeper) ApplicationRoleStateAll(c context.Context, req *types.QueryAllApplicationRoleStateRequest) (*types.QueryAllApplicationRoleStateResponse, error) {
	var applicationRoleStates []*types.ApplicationRoleState
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	applicationRoleStateStore := prefix.NewStore(store, types.KeyPrefix(types.ApplicationRoleStateKey))

	pageRes, err := query.Paginate(applicationRoleStateStore, req.Pagination, func(key []byte, value []byte) error {
		var applicationRoleState types.ApplicationRoleState
		if err := k.cdc.Unmarshal(value, &applicationRoleState); err != nil {
			return err
		}

		applicationRoleStates = append(applicationRoleStates, &applicationRoleState)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllApplicationRoleStateResponse{ApplicationRoleState: applicationRoleStates, Pagination: pageRes}, nil
}

func (k Keeper) ApplicationRoleState(c context.Context, req *types.QueryGetApplicationRoleStateRequest) (*types.QueryGetApplicationRoleStateResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)

	if !k.HasApplicationRoleState(ctx, req.Id) {
		return &types.QueryGetApplicationRoleStateResponse{}, sdkErrors.ErrKeyNotFound
	}
	applicationRoleState := k.GetApplicationRoleState(ctx, req.Id)

	return &types.QueryGetApplicationRoleStateResponse{ApplicationRoleState: &applicationRoleState}, nil
}
