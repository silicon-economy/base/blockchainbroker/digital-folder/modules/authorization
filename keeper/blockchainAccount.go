// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"
	"time"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

// GetBlockchainAccountCount gets the total number of blockchainAccount
func (k Keeper) GetBlockchainAccountCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountCountKey))
	byteKey := types.KeyPrefix(types.BlockchainAccountCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetBlockchainAccountCount sets the total number of blockchainAccount
func (k Keeper) SetBlockchainAccountCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountCountKey))
	byteKey := types.KeyPrefix(types.BlockchainAccountCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendBlockchainAccount appends a blockchainAccount in the store with a new id and updates the count
func (k Keeper) AppendBlockchainAccount(
	ctx sdk.Context,
	creator string,
	id string,
) (string, error) {
	// Create the blockchainAccount
	var BlockchainAccountState = types.BlockchainAccountState{
		Creator:            creator,
		Id:                 uuid.New().String(),
		ApplicationRoleIDs: []string{},
		AccountID:          id,
		Valid:              true,
		TimeStamp:          time.Now().UTC().Format(time.RFC3339),
	}
	var blockchainAccountStates []*types.BlockchainAccountState
	blockchainAccountStates = append(blockchainAccountStates, &BlockchainAccountState)
	var blockchainAccount = types.BlockchainAccount{
		Creator:                 creator,
		Id:                      id,
		BlockchainAccountStates: blockchainAccountStates,
	}

	k.SetBlockchainAccount(ctx, blockchainAccount)

	// Update blockchainAccount count
	count := k.GetBlockchainAccountCount(ctx)
	k.SetBlockchainAccountCount(ctx, count+1)

	return id, nil
}

// SetBlockchainAccount set a specific blockchainAccount in the store
func (k Keeper) SetBlockchainAccount(ctx sdk.Context, blockchainAccount types.BlockchainAccount) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountKey))
	b := k.cdc.MustMarshal(&blockchainAccount)
	store.Set(types.KeyPrefix(types.BlockchainAccountKey+blockchainAccount.Id), b)
}

// GetBlockchainAccount returns a blockchainAccount from its id
func (k Keeper) GetBlockchainAccount(ctx sdk.Context, key string) types.BlockchainAccount {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountKey))
	var blockchainAccount types.BlockchainAccount
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.BlockchainAccountKey+key)), &blockchainAccount)
	return blockchainAccount
}

// HasBlockchainAccount checks if the blockchainAccount exists in the store
func (k Keeper) HasBlockchainAccount(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountKey))
	return store.Has(types.KeyPrefix(types.BlockchainAccountKey + id))
}

// GetAllBlockchainAccount returns all blockchainAccounts
func (k Keeper) GetAllBlockchainAccount(ctx sdk.Context) (msgs []types.BlockchainAccount) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.BlockchainAccountKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.BlockchainAccount
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}

// HasRole checks if a user has a specific role and if the role is currently valid
func (k Keeper) HasRole(ctx sdk.Context, key string, applicationRole string) (bool, error) {
	if !k.GetConfiguration(ctx, 0).PermissionCheck {
		return true, nil
	}
	blockchainAccountState, err := k.GetLatestBlockchainAccountState(ctx, key)
	accounts := k.GetAllBlockchainAccount(ctx)
	_ = accounts
	if err != nil {
		return false, err
	}
	roles := blockchainAccountState.ApplicationRoleIDs

	for _, elem := range roles {
		if elem == applicationRole {
			// found role in application Roles of BlockchainAccount
			applicationRoleState, err := k.GetLatestApplicationRoleState(ctx, applicationRole)
			if err != nil {
				return false, err
			}
			if applicationRoleState.Valid {
				return true, nil
			}
		}
	}

	return false, nil
}

// GetLatestBlockchainAccountState returns the latest state of a blockchainAccount
func (k Keeper) GetLatestBlockchainAccountState(ctx sdk.Context, account string) (types.BlockchainAccountState, error) {
	if k.HasBlockchainAccount(ctx, account) {
		bcAccount := k.GetBlockchainAccount(ctx, account)
		return *bcAccount.BlockchainAccountStates[len(bcAccount.BlockchainAccountStates)-1], nil
	} else {
		return types.BlockchainAccountState{}, sdkErrors.ErrKeyNotFound
	}
}

// SetBlockchainAccountInvalid invalidates a blockchainAccount
func (k Keeper) SetBlockchainAccountInvalid(ctx sdk.Context, creator string, key string) error {
	state, err := k.GetLatestBlockchainAccountState(ctx, key)
	if err != nil {
		return err
	}
	state.Valid = false
	state.Creator = creator

	return k.AppendNewBlockchainAccountState(ctx, key, state)
}

// AppendNewBlockchainAccountState appends a new state to an existing blockchainAccount
func (k Keeper) AppendNewBlockchainAccountState(ctx sdk.Context, account string, state types.BlockchainAccountState) error {
	state.TimeStamp = time.Now().UTC().Format(time.RFC3339)
	state.Id = uuid.New().String()

	if !k.HasBlockchainAccount(ctx, account) {
		return sdkErrors.ErrKeyNotFound
	}

	bcAccount := k.GetBlockchainAccount(ctx, account)
	bcAccount.BlockchainAccountStates[len(bcAccount.BlockchainAccountStates)-1].Valid = false
	bcAccount.BlockchainAccountStates = append(bcAccount.BlockchainAccountStates, &state)
	k.SetBlockchainAccount(ctx, bcAccount)
	return nil
}

// GrantApplicationPermission grants permission to a specific application role
func (k Keeper) GrantApplicationPermission(ctx sdk.Context, account string, applicationRole string, creator string) error {
	state, err := k.GetLatestBlockchainAccountState(ctx, account)
	if err != nil {
		return err
	}

	for _, role := range state.ApplicationRoleIDs {
		if role == applicationRole {
			return sdkErrors.ErrInvalidRequest
		}
	}

	state.ApplicationRoleIDs = append(state.ApplicationRoleIDs, applicationRole)

	return k.AppendNewBlockchainAccountState(ctx, account, state)
}

// RevokeApplicationPermission removes a permission from a user's blockchainAccount
func (k Keeper) RevokeApplicationPermission(ctx sdk.Context, account string, applicationRole string, creator string) error {
	state, err := k.GetLatestBlockchainAccountState(ctx, account)
	if err != nil {
		return err
	}

	for i, role := range state.ApplicationRoleIDs {
		if role == applicationRole {
			state.ApplicationRoleIDs[i] = state.ApplicationRoleIDs[len(state.ApplicationRoleIDs)-1]
			state.ApplicationRoleIDs[len(state.ApplicationRoleIDs)-1] = ""
			state.ApplicationRoleIDs = state.ApplicationRoleIDs[:len(state.ApplicationRoleIDs)-1]
		}
	}

	return k.AppendNewBlockchainAccountState(ctx, account, state)
}
