// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func (k Keeper) BlockchainAccountAll(c context.Context, req *types.QueryAllBlockchainAccountRequest) (*types.QueryAllBlockchainAccountResponse, error) {
	var blockchainAccounts []*types.BlockchainAccount
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	blockchainAccountStore := prefix.NewStore(store, types.KeyPrefix(types.BlockchainAccountKey))

	pageRes, err := query.Paginate(blockchainAccountStore, req.Pagination, func(key []byte, value []byte) error {
		var blockchainAccount types.BlockchainAccount
		if err := k.cdc.Unmarshal(value, &blockchainAccount); err != nil {
			return err
		}

		blockchainAccounts = append(blockchainAccounts, &blockchainAccount)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllBlockchainAccountResponse{BlockchainAccount: blockchainAccounts, Pagination: pageRes}, nil
}

func (k Keeper) BlockchainAccount(c context.Context, req *types.QueryGetBlockchainAccountRequest) (*types.QueryGetBlockchainAccountResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	if !k.HasBlockchainAccount(ctx, req.Id) {
		return &types.QueryGetBlockchainAccountResponse{}, sdkErrors.ErrKeyNotFound
	}

	blockchainAccount := k.GetBlockchainAccount(ctx, req.Id)
	return &types.QueryGetBlockchainAccountResponse{BlockchainAccount: &blockchainAccount}, nil
}
