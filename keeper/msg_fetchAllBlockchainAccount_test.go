// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	authorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func Test_msgServer_fetchAllBlockchainAccount(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := authorizationKeeper.NewMsgServerImpl(*keeper)
	accounts, _ := keeper.BlockchainAccountAll(goCtx, &types.QueryAllBlockchainAccountRequest{})

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchAllBlockchainAccount
	}
	tests := []struct {
		name    string
		args    args
		want    *types.MsgFetchAllBlockchainAccountResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllBlockchainAccount{Creator: app.CreatorA}},
			want:    &types.MsgFetchAllBlockchainAccountResponse{Account: accounts.BlockchainAccount},
			wantErr: false,
		},
		{
			name:    app.UnsuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllBlockchainAccount{Creator: app.CreatorB}},
			want:    &types.MsgFetchAllBlockchainAccountResponse{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.FetchAllBlockchainAccount(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchAllBlockchainAccount() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchAllBlockchainAccount() = %v, want %v", got, tt.want)
			}
		})
	}
}
