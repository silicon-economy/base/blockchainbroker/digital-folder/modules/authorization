// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

// GetBlockchainAccountStateCount gets the total number of blockchainAccountState
func (k Keeper) GetBlockchainAccountStateCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountStateCountKey))
	byteKey := types.KeyPrefix(types.BlockchainAccountStateCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetBlockchainAccountStateCount sets the total number of blockchainAccountState
func (k Keeper) SetBlockchainAccountStateCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountStateCountKey))
	byteKey := types.KeyPrefix(types.BlockchainAccountStateCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendBlockchainAccountState appends a blockchainAccountState in the store with a new id and updates the count
func (k Keeper) AppendBlockchainAccountState(
	ctx sdk.Context,
	creator string,
	accountID string,
	timeStamp string,
	applicationRoleIDs []string,
	valid bool,
) string {
	// Create the blockchainAccountState
	count := k.GetBlockchainAccountStateCount(ctx)
	id := strconv.FormatInt(count, 10)
	var blockchainAccountState = types.BlockchainAccountState{
		Creator:            creator,
		Id:                 id,
		AccountID:          accountID,
		TimeStamp:          timeStamp,
		ApplicationRoleIDs: applicationRoleIDs,
		Valid:              valid,
	}

	k.SetBlockchainAccountState(ctx, blockchainAccountState)

	// Update blockchainAccountState count
	k.SetBlockchainAccountStateCount(ctx, count+1)

	return id
}

// SetBlockchainAccountState sets a specific blockchainAccountState in the store
func (k Keeper) SetBlockchainAccountState(ctx sdk.Context, blockchainAccountState types.BlockchainAccountState) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountStateKey))
	b := k.cdc.MustMarshal(&blockchainAccountState)
	store.Set(types.KeyPrefix(types.BlockchainAccountStateKey+blockchainAccountState.Id), b)
}

// GetBlockchainAccountState returns a blockchainAccountState from its id
func (k Keeper) GetBlockchainAccountState(ctx sdk.Context, key string) types.BlockchainAccountState {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountStateKey))
	var blockchainAccountState types.BlockchainAccountState
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.BlockchainAccountStateKey+key)), &blockchainAccountState)
	return blockchainAccountState
}

// HasBlockchainAccountState checks if the blockchainAccountState exists in the store
func (k Keeper) HasBlockchainAccountState(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountStateKey))
	return store.Has(types.KeyPrefix(types.BlockchainAccountStateKey + id))
}

// GetBlockchainAccountStateOwner returns the creator of the blockchainAccountState
func (k Keeper) GetBlockchainAccountStateOwner(ctx sdk.Context, key string) string {
	return k.GetBlockchainAccountState(ctx, key).Creator
}

// GetAllBlockchainAccountState returns all blockchainAccountState
func (k Keeper) GetAllBlockchainAccountState(ctx sdk.Context) (msgs []types.BlockchainAccountState) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.BlockchainAccountStateKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.BlockchainAccountStateKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.BlockchainAccountState
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}
