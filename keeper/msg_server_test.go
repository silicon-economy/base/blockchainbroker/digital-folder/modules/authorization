// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	authorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func Test_msgServer_createBlockchainAccount(t *testing.T) {
	testableApp, ctx := app.CreateTestInput()
	keeper := testableApp.AuthorizationKeeper
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := authorizationKeeper.NewMsgServerImpl(keeper)

	type args struct {
		goCtx context.Context
		msg   *types.MsgCreateBlockchainAccount
	}
	tests := []struct {
		name    string
		want    *types.MsgCreateBlockchainAccountResponse
		wantErr bool
		args    args
	}{
		{
			name:    app.SuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgCreateBlockchainAccount{Creator: app.CreatorA, Id: ""}},
			want:    &types.MsgCreateBlockchainAccountResponse{},
			wantErr: false,
		},
		{
			name:    app.UnsuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgCreateBlockchainAccount{Creator: app.CreatorB}},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.CreateBlockchainAccount(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.CreateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.CreateToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_msgServer_deactivateApplicationRole(t *testing.T) {
	testableApp, ctx := app.CreateTestInput()
	keeper := testableApp.AuthorizationKeeper
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := authorizationKeeper.NewMsgServerImpl(keeper)

	type args struct {
		goCtx context.Context
		msg   *types.MsgDeactivateApplicationRole
	}
	tests := []struct {
		name    string
		want    *types.MsgDeactivateApplicationRoleResponse
		wantErr bool
		args    args
	}{
		{
			name:    app.SuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgDeactivateApplicationRole{Creator: app.CreatorA, Id: types.AuthorizationGrantRole}},
			want:    &types.MsgDeactivateApplicationRoleResponse{},
			wantErr: false,
		},
		{
			name:    app.UnsuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgDeactivateApplicationRole{Creator: app.CreatorB}},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.DeactivateApplicationRole(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.CreateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.CreateToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_msgServer_updateApplicationRole(t *testing.T) {
	testableApp, ctx := app.CreateTestInput()
	keeper := testableApp.AuthorizationKeeper
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := authorizationKeeper.NewMsgServerImpl(keeper)

	type args struct {
		goCtx context.Context
		msg   *types.MsgUpdateApplicationRole
	}
	tests := []struct {
		name    string
		want    *types.MsgUpdateApplicationRoleResponse
		wantErr bool
		args    args
	}{
		{
			name:    app.SuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgUpdateApplicationRole{Creator: app.CreatorA, Id: types.AuthorizationGrantRole}},
			want:    &types.MsgUpdateApplicationRoleResponse{},
			wantErr: false,
		},
		{
			name:    app.UnsuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgUpdateApplicationRole{Creator: app.CreatorB}},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.UpdateApplicationRole(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.CreateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.CreateToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_msgServer_createApplicationRole(t *testing.T) {
	testableApp, ctx := app.CreateTestInput()
	keeper := testableApp.AuthorizationKeeper
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := authorizationKeeper.NewMsgServerImpl(keeper)

	type args struct {
		goCtx context.Context
		msg   *types.MsgCreateApplicationRole
	}
	tests := []struct {
		name    string
		want    *types.MsgCreateApplicationRoleResponse
		wantErr bool
		args    args
	}{
		{
			name:    app.SuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgCreateApplicationRole{Creator: app.CreatorA, Id: "newRole"}},
			want:    &types.MsgCreateApplicationRoleResponse{Id: "newRole"},
			wantErr: false,
		},
		{
			name:    app.UnsuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgCreateApplicationRole{Creator: app.CreatorB, Id: "newRole"}},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.CreateApplicationRole(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.CreateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.CreateToken() = %v, want %v", got, tt.want)
			}
		})
	}
}
