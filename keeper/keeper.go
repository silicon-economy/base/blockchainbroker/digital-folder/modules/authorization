// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/tendermint/tendermint/libs/log"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

type (
	Keeper struct {
		cdc      codec.Codec
		storeKey sdk.StoreKey
		memKey   sdk.StoreKey
	}
)

func NewKeeper(cdc codec.Codec, storeKey, memKey sdk.StoreKey) *Keeper {
	return &Keeper{
		cdc:      cdc,
		storeKey: storeKey,
		memKey:   memKey,
	}
}

func (k Keeper) Logger(ctx sdk.Context) log.Logger {
	return ctx.Logger().With("module", fmt.Sprintf("x/%s", types.ModuleName))
}

// InitGenesis initializes the capability module's state from a provided genesis state
func (k Keeper) InitGenesis(ctx sdk.Context, genState types.GenesisState) {
	// this line is used by starport scaffolding # genesis/module/init
	// Set all the configuration
	for _, elem := range genState.ConfigurationList {
		k.SetConfiguration(ctx, *elem)
	}
	// Set all the blockchainAccountState
	for _, elem := range genState.BlockchainAccountStateList {
		k.SetBlockchainAccountState(ctx, *elem)
	}

	// Set blockchainAccountState count
	k.SetBlockchainAccountStateCount(ctx, int64(len(genState.BlockchainAccountStateList)))

	// Set all the applicationRoleState
	for _, elem := range genState.ApplicationRoleStateList {
		k.SetApplicationRoleState(ctx, *elem)
	}

	// Set applicationRoleState count
	k.SetApplicationRoleStateCount(ctx, int64(len(genState.ApplicationRoleStateList)))

	// Set all the blockchainAccount
	for _, elem := range genState.BlockchainAccountList {
		k.SetBlockchainAccount(ctx, *elem)
	}

	// Set blockchainAccount count
	k.SetBlockchainAccountCount(ctx, int64(len(genState.BlockchainAccountList)))

	// Set all the applicationRole
	for _, elem := range genState.ApplicationRoleList {
		k.SetApplicationRole(ctx, *elem)
	}

	// Set applicationRole count
	k.SetApplicationRoleCount(ctx, int64(len(genState.ApplicationRoleList)))

}

// RevertToGenesis reverts to genesis state
func (k Keeper) RevertToGenesis(ctx sdk.Context) {
	store := ctx.KVStore(k.storeKey)
	it := store.Iterator(nil, nil)
	defer it.Close()

	for ; it.Valid(); it.Next() {
		store.Delete(it.Key())
	}

	k.InitGenesis(ctx, *types.DefaultGenesis())
}

func GetTimestamp() string {
	return time.Now().UTC().Format(time.RFC3339)
}
