// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

// GetApplicationRoleStateCount gets the total number of applicationRoleState
func (k Keeper) GetApplicationRoleStateCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleStateCountKey))
	byteKey := types.KeyPrefix(types.ApplicationRoleStateCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetApplicationRoleStateCount sets the total number of applicationRoleState
func (k Keeper) SetApplicationRoleStateCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleStateCountKey))
	byteKey := types.KeyPrefix(types.ApplicationRoleStateCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendApplicationRoleState appends an applicationRoleState in the store with a new id and updates the count
func (k Keeper) AppendApplicationRoleState(
	ctx sdk.Context,
	creator string,
	applicationRoleID string,
	description string,
	valid bool,
	timeStamp string,
) string {
	// Create the applicationRoleState
	count := k.GetApplicationRoleStateCount(ctx)
	id := strconv.FormatInt(count, 10)
	var applicationRoleState = types.ApplicationRoleState{
		Creator:           creator,
		Id:                id,
		ApplicationRoleID: applicationRoleID,
		Description:       description,
		Valid:             valid,
		TimeStamp:         timeStamp,
	}

	k.SetApplicationRoleState(ctx, applicationRoleState)
	// Update applicationRoleState count
	k.SetApplicationRoleStateCount(ctx, count+1)

	return id
}

// SetApplicationRoleState sets a specific applicationRoleState in the store
func (k Keeper) SetApplicationRoleState(ctx sdk.Context, applicationRoleState types.ApplicationRoleState) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleStateKey))
	b := k.cdc.MustMarshal(&applicationRoleState)
	store.Set(types.KeyPrefix(types.ApplicationRoleStateKey+applicationRoleState.Id), b)
}

// GetApplicationRoleState returns a applicationRoleState from its id
func (k Keeper) GetApplicationRoleState(ctx sdk.Context, key string) types.ApplicationRoleState {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleStateKey))
	var applicationRoleState types.ApplicationRoleState
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.ApplicationRoleStateKey+key)), &applicationRoleState)
	return applicationRoleState
}

// HasApplicationRoleState checks if the applicationRoleState exists in the store
func (k Keeper) HasApplicationRoleState(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleStateKey))
	return store.Has(types.KeyPrefix(types.ApplicationRoleStateKey + id))
}

// GetAllApplicationRoleState returns all applicationRoleState
func (k Keeper) GetAllApplicationRoleState(ctx sdk.Context) (msgs []types.ApplicationRoleState) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ApplicationRoleStateKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.ApplicationRoleStateKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.ApplicationRoleState
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}
