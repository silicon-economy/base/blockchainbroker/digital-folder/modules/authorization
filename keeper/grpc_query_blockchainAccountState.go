// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func (k Keeper) BlockchainAccountStateAll(c context.Context, req *types.QueryAllBlockchainAccountStateRequest) (*types.QueryAllBlockchainAccountStateResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)

	var blockchainAccountStates []*types.BlockchainAccountState
	store := ctx.KVStore(k.storeKey)
	blockchainAccountStateStore := prefix.NewStore(store, types.KeyPrefix(types.BlockchainAccountStateKey))

	pageRes, err := query.Paginate(blockchainAccountStateStore, req.Pagination, func(key []byte, value []byte) error {
		var blockchainAccountState types.BlockchainAccountState
		if err := k.cdc.Unmarshal(value, &blockchainAccountState); err != nil {
			return err
		}

		blockchainAccountStates = append(blockchainAccountStates, &blockchainAccountState)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllBlockchainAccountStateResponse{BlockchainAccountState: blockchainAccountStates, Pagination: pageRes}, nil
}

func (k Keeper) BlockchainAccountState(c context.Context, req *types.QueryGetBlockchainAccountStateRequest) (*types.QueryGetBlockchainAccountStateResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	if !k.HasBlockchainAccountState(ctx, req.Id) {
		return &types.QueryGetBlockchainAccountStateResponse{}, sdkErrors.ErrKeyNotFound
	}

	blockchainAccountState := k.GetBlockchainAccountState(ctx, req.Id)
	return &types.QueryGetBlockchainAccountStateResponse{BlockchainAccountState: &blockchainAccountState}, nil
}
