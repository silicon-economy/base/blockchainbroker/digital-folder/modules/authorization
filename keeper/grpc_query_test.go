// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"fmt"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func (suite *AuthorizationTestSuite) TestGRPCApplicationRole() {
	suite.SetupAdminAccount()
	suite.SetupApplicationRoles()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	applicationRole, _ := testableApp.AuthorizationKeeper.GetApplicationRole(ctx, appRoleID)
	roles := testableApp.AuthorizationKeeper.GetAllApplicationRole(ctx)
	_ = roles
	blockchainAccounts := testableApp.AuthorizationKeeper.GetAllBlockchainAccount(ctx)
	_ = blockchainAccounts
	suite.Require().NotNil(applicationRole)

	var req *types.QueryGetApplicationRoleRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetApplicationRoleRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetApplicationRoleRequest{Id: types.AuthorizationBlockAccount}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.ApplicationRole(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
				suite.Equal(applicationRole.Id, res.ApplicationRole.Id)
			} else {
				suite.Require().Nil(res)
			}
		})
	}
}

func (suite *AuthorizationTestSuite) TestGRPCApplicationRoleAll() {
	suite.SetupAdminAccount()
	suite.SetupApplicationRoles()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	applicationRoleAll := testableApp.AuthorizationKeeper.GetAllApplicationRole(ctx)
	suite.Require().NotNil(applicationRoleAll)

	var req *types.QueryAllApplicationRoleRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllApplicationRoleRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.ApplicationRoleAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Require().Nil(res)
			}
		})
	}
}

func (suite *AuthorizationTestSuite) TestGRPCBlockchainAccount() {
	suite.SetupAdminAccount()
	suite.SetupApplicationRoles()
	suite.SetupAccount()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	blockchainAccount := testableApp.AuthorizationKeeper.GetBlockchainAccount(ctx, app.CreatorA)
	suite.Require().NotNil(blockchainAccount)

	var req *types.QueryGetBlockchainAccountRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetBlockchainAccountRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetBlockchainAccountRequest{Id: app.CreatorA}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.BlockchainAccount(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
				suite.Equal(blockchainAccount.Id, res.BlockchainAccount.Id)
			} else {
				suite.Require().Nil(res)
			}
		})
	}
}

func (suite *AuthorizationTestSuite) TestGRPCBlockchainAccountAll() {
	suite.SetupAdminAccount()
	suite.SetupApplicationRoles()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	blockchainAccountAll := testableApp.AuthorizationKeeper.GetAllBlockchainAccount(ctx)
	suite.Require().NotNil(blockchainAccountAll)

	var req *types.QueryAllBlockchainAccountRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllBlockchainAccountRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.BlockchainAccountAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Require().Nil(res)
			}
		})
	}
}

func (suite *AuthorizationTestSuite) TestGRPCApplicationRoleState() {
	suite.SetupApplicationRoleState()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	applicationRoleState := testableApp.AuthorizationKeeper.GetApplicationRoleState(ctx, appRoleStateID)
	suite.Require().NotNil(applicationRoleState)

	var req *types.QueryGetApplicationRoleStateRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetApplicationRoleStateRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetApplicationRoleStateRequest{Id: appRoleStateID}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.ApplicationRoleState(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
				suite.Equal(applicationRoleState.Id, res.ApplicationRoleState.Id)
			} else {
				suite.Nil(res)
			}
		})
	}
}

func (suite *AuthorizationTestSuite) TestGRPCApplicationRoleStateAll() {
	suite.SetupApplicationRoleState()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	applicationRoleStateAll := testableApp.AuthorizationKeeper.GetAllApplicationRoleState(ctx)
	suite.Require().NotNil(applicationRoleStateAll)

	var req *types.QueryAllApplicationRoleStateRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllApplicationRoleStateRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.ApplicationRoleStateAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Require().Equal(res.ApplicationRoleState, []*types.ApplicationRoleState{})
			}
		})
	}
}

func (suite *AuthorizationTestSuite) TestGRPCBlockchainAccountState() {
	suite.SetupBlockchainAccountState()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	blockchainAccountState := testableApp.AuthorizationKeeper.GetBlockchainAccountState(ctx, blockAccStateID)
	suite.Require().NotNil(blockchainAccountState)

	var req *types.QueryGetBlockchainAccountStateRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetBlockchainAccountStateRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetBlockchainAccountStateRequest{Id: blockAccStateID}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.BlockchainAccountState(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
				suite.Equal(blockchainAccountState.Id, res.BlockchainAccountState.Id)
			} else {
				suite.Nil(res)
			}
		})
	}
}

func (suite *AuthorizationTestSuite) TestGRPCBlockchainAccountStateAll() {
	suite.SetupBlockchainAccountState()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	blockchainAccountStateAll := testableApp.AuthorizationKeeper.GetAllBlockchainAccountState(ctx)
	suite.Require().NotNil(blockchainAccountStateAll)

	var req *types.QueryAllBlockchainAccountStateRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllBlockchainAccountStateRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.BlockchainAccountStateAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Require().Equal(res.BlockchainAccountState, []*types.ApplicationRoleState{})
			}
		})
	}
}
