// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	authorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func Test_msgServer_fetchAllApplicationRole(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := authorizationKeeper.NewMsgServerImpl(*keeper)
	roles, _ := keeper.ApplicationRoleAll(goCtx, &types.QueryAllApplicationRoleRequest{})

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchAllApplicationRole
	}
	tests := []struct {
		name    string
		args    args
		want    *types.MsgFetchAllApplicationRoleResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllApplicationRole{Creator: app.CreatorA}},
			want:    &types.MsgFetchAllApplicationRoleResponse{Role: roles.ApplicationRole},
			wantErr: false,
		},
		{
			name:    app.UnsuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgFetchAllApplicationRole{Creator: app.CreatorB}},
			want:    &types.MsgFetchAllApplicationRoleResponse{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.FetchAllApplicationRole(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchAllApplicationRole() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchAllApplicationRole() = %v, want %v", got, tt.want)
			}
		})
	}
}
