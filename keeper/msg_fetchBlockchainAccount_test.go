// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	authorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func Test_msgServer_fetchBlockchainAccount(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := authorizationKeeper.NewMsgServerImpl(*keeper)
	account, _ := keeper.BlockchainAccount(goCtx, &types.QueryGetBlockchainAccountRequest{})

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchBlockchainAccount
	}
	tests := []struct {
		name    string
		args    args
		want    *types.MsgFetchBlockchainAccountResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgFetchBlockchainAccount{Creator: app.CreatorA}},
			want:    &types.MsgFetchBlockchainAccountResponse{Account: account.BlockchainAccount},
			wantErr: false,
		},
		{
			name:    app.UnsuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgFetchBlockchainAccount{Creator: app.CreatorB}},
			want:    &types.MsgFetchBlockchainAccountResponse{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.FetchBlockchainAccount(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchBlockchainAccount() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchBlockchainAccount() = %v, want %v", got, tt.want)
			}
		})
	}
}
