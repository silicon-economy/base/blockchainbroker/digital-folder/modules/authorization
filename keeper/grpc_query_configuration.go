// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func (k Keeper) Configuration(c context.Context, req *types.QueryGetConfigurationRequest) (*types.QueryGetConfigurationResponse, error) {
	var configuration types.Configuration
	ctx := sdk.UnwrapSDKContext(c)

	if !k.HasConfiguration(ctx, req.Id) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ConfigurationKey))
	k.cdc.MustUnmarshal(store.Get(GetConfigurationIDBytes(req.Id)), &configuration)

	return &types.QueryGetConfigurationResponse{Configuration: &configuration}, nil
}
