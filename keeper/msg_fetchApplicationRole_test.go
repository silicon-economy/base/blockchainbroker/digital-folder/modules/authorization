// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	authorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
)

func Test_msgServer_fetchApplicationRole(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	msgServer := authorizationKeeper.NewMsgServerImpl(*keeper)
	role, _ := keeper.ApplicationRole(goCtx, &types.QueryGetApplicationRoleRequest{Id: "0"})

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchApplicationRole
	}
	tests := []struct {
		name    string
		args    args
		want    *types.MsgFetchApplicationRoleResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgFetchApplicationRole{Creator: app.CreatorA}},
			want:    &types.MsgFetchApplicationRoleResponse{Role: role.ApplicationRole},
			wantErr: false,
		},
		{
			name:    app.UnsuccessfulTx,
			args:    args{goCtx: goCtx, msg: &types.MsgFetchApplicationRole{Creator: app.CreatorB}},
			want:    &types.MsgFetchApplicationRoleResponse{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := msgServer.FetchApplicationRole(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchApplicationRole() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchApplicationRole() = %v, want %v", got, tt.want)
			}
		})
	}
}
