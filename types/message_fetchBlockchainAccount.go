// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgFetchBlockchainAccount{}

func NewMsgFetchBlockchainAccount(creator string, id string) *MsgFetchBlockchainAccount {
	return &MsgFetchBlockchainAccount{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgFetchBlockchainAccount) Route() string {
	return RouterKey
}

func (msg *MsgFetchBlockchainAccount) Type() string {
	return "MsgFetchBlockchainAccount"
}

func (msg *MsgFetchBlockchainAccount) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchBlockchainAccount) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchBlockchainAccount) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
