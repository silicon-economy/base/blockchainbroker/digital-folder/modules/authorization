// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateBlockchainAccount{}

func NewMsgCreateBlockchainAccount(creator string, id string) *MsgCreateBlockchainAccount {
	return &MsgCreateBlockchainAccount{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgCreateBlockchainAccount) Route() string {
	return RouterKey
}

func (msg *MsgCreateBlockchainAccount) Type() string {
	return "CreateBlockchainAccount"
}

func (msg *MsgCreateBlockchainAccount) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateBlockchainAccount) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateBlockchainAccount) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgGrantAppRoleToBlockchainAccount{}

func NewMsgGrantAppRoleToBlockchainAccount(creator string, user string, roleID string) *MsgGrantAppRoleToBlockchainAccount {
	return &MsgGrantAppRoleToBlockchainAccount{
		Creator: creator,
		User:    user,
		RoleID:  roleID,
	}
}
func (msg *MsgGrantAppRoleToBlockchainAccount) Route() string {
	return RouterKey
}

func (msg *MsgGrantAppRoleToBlockchainAccount) Type() string {
	return "GrantAppRoleToBlockchainAccount"
}

func (msg *MsgGrantAppRoleToBlockchainAccount) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgGrantAppRoleToBlockchainAccount) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgGrantAppRoleToBlockchainAccount) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgRevokeAppRoleFromBlockchainAccount{}

func NewMsgRevokeAppRoleFromBlockchainAccount(creator string, account string, roleID string) *MsgRevokeAppRoleFromBlockchainAccount {
	return &MsgRevokeAppRoleFromBlockchainAccount{
		Creator: creator,
		Account: account,
		RoleID:  roleID,
	}
}
func (msg *MsgRevokeAppRoleFromBlockchainAccount) Route() string {
	return RouterKey
}

func (msg *MsgRevokeAppRoleFromBlockchainAccount) Type() string {
	return "RevokeAppRoleFromBlockchainAccount"
}

func (msg *MsgRevokeAppRoleFromBlockchainAccount) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgRevokeAppRoleFromBlockchainAccount) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgRevokeAppRoleFromBlockchainAccount) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeactivateBlockchainAccount{}

func NewMsgDeactivateBlockchainAccount(creator string, id string) *MsgDeactivateBlockchainAccount {
	return &MsgDeactivateBlockchainAccount{
		Creator: creator,
		Id:      id,
	}
}
func (msg *MsgDeactivateBlockchainAccount) Route() string {
	return RouterKey
}

func (msg *MsgDeactivateBlockchainAccount) Type() string {
	return "RevokeAppRoleFromBlockchainAccount"
}

func (msg *MsgDeactivateBlockchainAccount) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeactivateBlockchainAccount) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeactivateBlockchainAccount) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
