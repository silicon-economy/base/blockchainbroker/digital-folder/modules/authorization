// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"fmt"
	"time"

	"github.com/google/uuid"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {

	var genesisAccount = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"
	var initialRoleIds = []string{
		AuthorizationBlockAccount,
		AuthorizationCreateAccount,
		AuthorizationCreateApplicationRole,
		AuthorizationDeactivateApplicationRole,
		AuthorizationGrantRole,
		AuthorizationReadApplicationRole,
		AuthorizationRevokeRole,
		AuthorizationUpdateApplicationRole}
	var initialApplicationRoles = []*ApplicationRole{}
	for _, applicationRoleId := range initialRoleIds {
		initialApplicationRoles = append(initialApplicationRoles, &ApplicationRole{
			Creator: genesisAccount,
			Id:      applicationRoleId,
			ApplicationRoleStates: []*ApplicationRoleState{
				{
					Creator:           genesisAccount,
					Id:                uuid.New().String(),
					ApplicationRoleID: applicationRoleId,
					Description:       applicationRoleId,
					Valid:             true,
					TimeStamp:         time.Now().UTC().Format(time.RFC3339),
				},
			},
		})
	}

	return &GenesisState{
		// this line is used by starport scaffolding # genesis/types/default
		ConfigurationList:          []*Configuration{{Creator: "Genesis", Id: 0, PermissionCheck: false}},
		BlockchainAccountStateList: []*BlockchainAccountState{},
		ApplicationRoleStateList:   []*ApplicationRoleState{},
		BlockchainAccountList: []*BlockchainAccount{
			{
				Creator: genesisAccount,
				Id:      genesisAccount,
				BlockchainAccountStates: []*BlockchainAccountState{
					{
						Creator: genesisAccount,
						Id:      uuid.New().String(), AccountID: genesisAccount,
						TimeStamp:          time.Now().UTC().Format(time.RFC3339),
						ApplicationRoleIDs: initialRoleIds, Valid: true,
					},
				},
			},
		},
		ApplicationRoleList: initialApplicationRoles,
	}
}

// Validate performs basic genesis state validation returning an error upon any
// failure.
func (gs GenesisState) Validate() error {
	// this line is used by starport scaffolding # genesis/types/validate
	// Check for duplicated ID in configuration
	configurationIdMap := make(map[uint64]bool)

	for _, elem := range gs.ConfigurationList {
		if _, ok := configurationIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for configuration")
		}
		configurationIdMap[elem.Id] = true
	}
	// Check for duplicated ID in blockchainAccountState
	blockchainAccountStateIdMap := make(map[string]bool)

	for _, elem := range gs.BlockchainAccountStateList {
		if _, ok := blockchainAccountStateIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for blockchainAccountState")
		}
		blockchainAccountStateIdMap[elem.Id] = true
	}
	// Check for duplicated ID in applicationRoleState
	applicationRoleStateIdMap := make(map[string]bool)

	for _, elem := range gs.ApplicationRoleStateList {
		if _, ok := applicationRoleStateIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for applicationRoleState")
		}
		applicationRoleStateIdMap[elem.Id] = true
	}
	// Check for duplicated ID in blockchainAccount
	blockchainAccountIdMap := make(map[string]bool)

	for _, elem := range gs.BlockchainAccountList {
		if _, ok := blockchainAccountIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for blockchainAccount")
		}
		blockchainAccountIdMap[elem.Id] = true
	}
	// Check for duplicated ID in applicationRole
	applicationRoleIdMap := make(map[string]bool)

	for _, elem := range gs.ApplicationRoleList {
		if _, ok := applicationRoleIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for applicationRole")
		}
		applicationRoleIdMap[elem.Id] = true
	}

	return nil
}
